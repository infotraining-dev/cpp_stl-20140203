#include "utils.hpp"

#include <vector>
#include <random>
#include <set>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>
#include <boost/bind.hpp>

using namespace std;

class RandGen
{
    int range_;
    random_device* rd;
    mt19937 *gen;
public:
    RandGen(int range) : range_(range)
    {
        rd = new random_device;
        gen = new mt19937((*rd)());
    }

    int operator()() const
    {
        uniform_int_distribution<> dis(0, range_);
        return dis(*gen);
    }
};

int main()
{
    vector<int> vec(25);
    generate(vec.begin(), vec.end(), RandGen(30));

    vector<int> sorted_vec(vec);
    sort(sorted_vec.begin(), sorted_vec.end());

    multiset<int> mset(vec.begin(), vec.end());

    print(vec, "vec: ");
    print(sorted_vec, "sorted_vec: ");
    print(mset, "mset: ");

    // czy 14 znajduje sie w sekwencji?
    cout << "\nczy 17 znajduje sie w sekwencji?" << endl;
    cout << (find(vec.begin(), vec.end(), 17) != vec.end()) << endl;
    cout << binary_search(sorted_vec.begin(), sorted_vec.end(), 17) << endl;
    cout << (mset.find(17) != mset.end()) << endl;

    cout << "\n\nile razy wystepuje 22\n";
    // ile razy wystepuje 22
    cout << count(vec.begin(), vec.end(), 22) << endl;

    pair<vector<int>::iterator, vector<int>::iterator> range22
            = equal_range(sorted_vec.begin(), sorted_vec.end(), 22);
    cout << (range22.second - range22.first) << endl;

    cout << mset.count(22) << endl;

    // usun wszystkie wieksze od 15
    vec.erase(remove_if(vec.begin(), vec.end(), boost::bind(greater<int>(), _1, 15)),
              vec.end());
    sorted_vec.erase(upper_bound(sorted_vec.begin(), sorted_vec.end(), 15), sorted_vec.end());
    mset.erase(mset.upper_bound(15), mset.end());
}
