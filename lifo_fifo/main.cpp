#include <iostream>
#include <stdexcept>
#include <stack>
#include <vector>
#include <queue>

using namespace std;

class Problem
{
    int value_;
public:
    Problem(int value) : value_(value) {}

    Problem(const Problem& source)
    {
        static int counter = 0;
        if (++counter == 6)
        {
            throw runtime_error("Error in ctor...");
        }

        value_ = source.value();
    }

    Problem& operator=(const Problem& source)
    {
        static int counter = 0;
        if (++counter == 6)
        {
            cout << "Error in assignment operator..." << endl;
            throw runtime_error("Error in assignment...");
        }

        value_ = source.value();

        return *this;
    }

    int value() const
    {
        return value_;
    }
};

class Person
{
public:
    int id;
    string name;

    Person(int id, const string& name) : id(id), name(name)
    {}

    bool operator<(const Person& other) const
    {
        return this->id < other.id;
    }
};

ostream& operator<<(ostream& out, const Person& p)
{
    out << "Id: " << p.id << " Name: " << p.name;
    return out;
}

int main()
{
    // stack interface - strong guanrantee
    stack<Problem> stack_problem;

    stack_problem.push(1);
    stack_problem.push(2);
    stack_problem.push(3);
    stack_problem.push(4);

    cout << "size of stack:" << stack_problem.size() << endl;

    try
    {
        while(!stack_problem.empty())
        {
            cout << "Before pop - size of stack = " << stack_problem.size() << endl;
            Problem x = stack_problem.top(); // exception
            stack_problem.pop();

            cout << x.value() << " popped!" << endl;
        }
    }
    catch(runtime_error& e)
    {
        cout << e.what() << endl;
        cout << "After exception - size of stack = " << stack_problem.size() << endl;
    }


    // kolejka priorytetowa
    priority_queue<int> pq;

    pq.push(4);
    pq.push(3);
    pq.push(9);
    pq.push(1);

    cout << "\n\n";

    while(!pq.empty())
    {
        cout << pq.top();
        pq.pop();
        cout << " - item popped!\n";
    }


    cout << "\n\n";

    priority_queue<Person> queue_for_coffee;

    queue_for_coffee.push(Person(8, "Kowalski"));
    queue_for_coffee.push(Person(1, "Kowal"));
    queue_for_coffee.push(Person(6, "Kowalewski"));

    while(!queue_for_coffee.empty())
    {
        cout << queue_for_coffee.top() << endl;
        queue_for_coffee.pop();
    }
}

