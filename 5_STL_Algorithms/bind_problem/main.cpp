#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>
#include <boost/bind.hpp>
#include <functional>

using namespace std;

void foo(int x, const string& txt, double dx)
{
    cout << "foo(" << x << ", " << txt <<  ", " << dx << ")" << endl;
}


class Person
{
public:
    int id_;
    string name_;

    Person(int id, const string& name) : id_(id), name_(name)
    {}
    void print(const string& prefix) const
    {
        cout << prefix << " Person(id = " << id_ << ", name = " << name_ << ")" << endl;
    }
};

class Add : public binary_function<int, int, int>
{
public:
    int operator()(int x, int y) const
    {
        return x + y;
    }
};

int main()
{
    auto bound_foo = boost::bind(&foo, 13, _1, 3.14); // _1 to bedzie opozniony argument podany w momencie wywolania funktora

    bound_foo("text"); // rownowazne foo(13, "text", 3.14);
    bound_foo("another_text"); // rownowazne foo(13, "another_text", 3.14);

    Add a;

    auto bound_add = boost::bind(a, _1, 2);
    cout << bound_add(10) << endl;

    Person p(2, "Kowalski");
    auto bound_method = boost::bind(&Person::print, &p, "Person: ");
    bound_method();

    auto lazy_method = boost::bind(&Person::print, _1, "Person: "); //c++11: zamiast _1 placeholders::_1
    lazy_method(p);

}
