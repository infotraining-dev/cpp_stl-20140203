#include "utils.hpp"

#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/algorithm/cxx11/copy_if.hpp>

using namespace std;

class RandGen
{
    int range_;
public:
    RandGen(int range) : range_(range)
    {
    }

    int operator()() const
    {
        return rand() % range_;
    }
};

int main()
{
    vector<int> vec(25);

    generate(vec.begin(), vec.end(), RandGen(30));

    print(vec, "vec: ");

    // 1a - wyświetl parzyste
    cout << "Parzyste: ";
    //remove_copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "), bind2nd(modulus<int>(), 2));
    remove_copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "),
                   boost::bind(modulus<int>(), _1, 2));
    cout << "\n";

    // 1b - wyswietl ile jest nieparzystych
    //cout << "Ilosc nieparzystych: " << count_if(vec.begin(), vec.end(), bind2nd(modulus<int>(), 2)) << endl;
    boost::function<bool (int)> is_even = boost::bind(modulus<int>(), _1, 2);

    cout << "Ilosc nieparzystych: " << count_if(vec.begin(), vec.end(), is_even) << endl;

    // 1c - wyswietl ile jest parzystych
    cout << "Ilosc parzystych: " << count_if(vec.begin(), vec.end(),
                                             std::bind(
                                                 logical_not<bool>(),
                                                 std::bind(modulus<int>(), placeholders::_1, 2))) << endl;

    // 2 - usuń liczby podzielne przez 3
    vec.erase(remove_if(vec.begin(), vec.end(), not1(bind2nd(modulus<int>(), 3))), vec.end());
    print(vec, "vec po usunieciu podzielnych przez 3: ");

    // 3 - tranformacja: podnieś liczby do kwadratu
    //transform(vec.begin(), vec.end(), vec.begin(), vec.begin(), multiplies<int>());
    double (*f)(double, double) = &pow;
    transform(vec.begin(), vec.end(), vec.begin(), bind2nd(ptr_fun(f), 2));
    print(vec, "kwadraty");

    // 4 - wypisz 5 najwiekszych liczb
    nth_element(vec.begin(), vec.begin() + 5,  vec.end(), greater<int>());
    cout << "5 największych: ";
    copy(vec.begin(), vec.begin() + 5, ostream_iterator<int>(cout, " "));
    cout << "\n";

    // 5 - policz wartosc srednia
    cout << "Avg: "
         << accumulate(vec.begin(), vec.end(), 0) / static_cast<double>(vec.size())
         << endl;

    // 6 - wyswietl wszystkie mniejsze od 50
    cout << "< 50: ";
    boost::algorithm::copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "),
                              bind2nd(less<int>(), 50));
    cout << "\n";
}
