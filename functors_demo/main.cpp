#include <iostream>
#include <vector>
#include <string>
#include <functional>
#include <boost/assign.hpp>
#include <boost/checked_delete.hpp>
#include <boost/mem_fn.hpp>
#include <boost/function.hpp>

using namespace std;

template <typename InIt, typename Func1>
Func1 mfor_each(InIt start, InIt end, Func1 f)
{
    while(start != end)
    {
        f(*(start++));
    }

    return f;
}

void print(const string& str)
{
    cout << "item: " << str << endl;
}

class Printer
{
public:
    void operator()(const string& str) const
    {
        cout << "printed item: " << str << endl;
    }
};

class IsEven : public unary_function<int, bool>
{
public:
    bool operator()(int val) const
    {
        return val % 2 == 0;
    }
};

bool is_even(int n)
{
    return n % 2 == 0;
}

class Person
{
public:
    int id;
    string name;

    Person(int id, const string& name) : id(id), name(name)
    {}

    void print()
    {
        cout << "Person(id=" << id << ", name=" << name << ")\n";
    }
};


int main()
{
    using namespace boost::assign;

    vector<string> vec_str;
    vec_str += "one", "two", "three";

    mfor_each(vec_str.begin(), vec_str.end(), &print);

    mfor_each(vec_str.begin(), vec_str.end(), Printer());

    vector<int> numbers;
    numbers += 9, 5, 7, 6, 1, 3, 9;

    //vector<int>::iterator where = find_if(numbers.begin(), numbers.end(), not1(IsEven()));
    vector<int>::iterator where = find_if(numbers.begin(), numbers.end(), not1(ptr_fun(&is_even)));

    if (where != numbers.end())
        cout << "Parzysta: " << *where << endl;
    else
        cout << "Nie ma przystych." << endl;


    vector<Person> vec_person;
    vec_person += Person(1, "Kowalski"), Person(2, "Nowak"), Person(3, "Adamski");

    //for_each(vec_person.begin(), vec_person.end(), mem_fun_ref(&Person::print));
    for_each(vec_person.begin(), vec_person.end(), boost::mem_fn(&Person::print)); // c++11

    cout << "\n\n";

    vector<Person*> vec_ptr_person;
    vec_ptr_person += new Person(1, "Kowalski"), new Person(2, "Nowak"), new Person(3, "Adamski");

    //for_each(vec_ptr_person.begin(), vec_ptr_person.end(), mem_fun(&Person::print));
    for_each(vec_ptr_person.begin(), vec_ptr_person.end(), boost::mem_fn(&Person::print));

    for_each(vec_ptr_person.begin(), vec_ptr_person.end(), &boost::checked_delete<Person>);

    // funktory wiążące
    cout << "\nFunktory wiążące" << endl;

    greater<int> gt;

    cout << "5 > 10 = " << gt(5, 10) << endl;

    boost::function<bool (int)> gt_than_10 = bind2nd(gt, 10);

    cout << "5 > 10 = " << gt_than_10(5) << endl;
}

