#include <iostream>
#include <stdexcept>
#include <vector>

// pre vs post

class Iter
{
   // ptr na element
public:
    // ++it
    Iter& operator++()
    {
        //++ptr;

        return *this;
    }

    // it++
    const Iter operator++(int)
    {
        Iter temp(*this); // kopia stanu iteratora
        //++ptr;

        return temp;
    }
};

using namespace std;

template <typename T>
void print_size_and_capacity(const vector<T>& vec, const string& prefix = "")
{
    cout << prefix << ": size = " << vec.size() << " " << ", capacity = " << vec.capacity() << endl;
}

template <typename Container>
void print(const Container& cont, const string& prefix)
{
    cout << prefix << ": [ ";
    for(typename Container::const_iterator it = cont.begin(); it != cont.end(); it++)
        cout << *it << " ";
    cout << "]" << endl;
}

class Person
{
public:
    int id;
    string name;

    Person(int id, const string& name) : id(id), name(name)
    {}
};

ostream& operator<<(ostream& out, const Person& p)
{
    out << "Id: " << p.id << " Name: " << p.name;
    return out;
}



int main()
{
    // konstruktory
    vector<int> vec_int1;

    print_size_and_capacity(vec_int1, "vec_int1");

    vector<int> vec_int2(10);

    print_size_and_capacity(vec_int2, "vec_int2");

    print(vec_int2, "vec_int2");

    vector<int> vec_int3(10, -1);

    print(vec_int3, "vec_int3");

    vector<int> copy_vec_int3(vec_int3.begin(), vec_int3.end());

    print(copy_vec_int3, "copy_vec_int3");

    int tab1[5] = { 1, 2, 3, 4, 5 };

    vector<int> vec_int4(tab1, tab1 + 5);

    print(vec_int4, "vec_int4");

    cout << "vec<int>.max_size() = " << vec_int4.max_size() << endl;

    // C++11 - lista inicjalizacyjna
    vector<string> words1 { "one", "two", "three" };
    //words1.reserve(1024);

    cout << "vec<string>.max_size() = " << words1.max_size() << endl;

    //vector<string> large_vec(words1.max_size() - 1);  // exception! bad_alloc

    print(words1, "words1");

    // dostęp do elementów
    cout << words1[1] << endl;
    words1[0] = "jeden";

    //words1[100] = "sto"; // error - undefined behaviour

    try
    {
        words1.at(100) = "sto";
    }
    catch(const out_of_range& e)
    {
        cout << e.what() << endl;
    }

    // iteracja

    cout << "iteracja wsteczna:\n";
    for(vector<string>::const_reverse_iterator rit = words1.rbegin(); rit != words1.rend(); ++rit)
    {
        cout << *rit << endl;
    }


    cout << "iteracja wsteczna:\n";
    for(auto rit = words1.rbegin(); rit != words1.rend(); ++rit)
    {
        cout << *rit << endl;
    }

    cout << "\n";

    // wstawianie elementów

    print_size_and_capacity(words1, "words1");
    for(int i = 0; i < 128; ++i)
    {
        words1.push_back("test");
        print_size_and_capacity(words1, "words1");
    }

    words1.insert(words1.begin(), "zero");

    words1.reserve(1024);

    print_size_and_capacity(words1, "words1 afer reserve");

    words1.erase(words1.begin() + 2, words1.end());

    print_size_and_capacity(words1, "words1 afer reserve");

    words1.reserve(32);

    print_size_and_capacity(words1, "words1 afer reserve");

    // zwolnienie bufora w wektorze
    vector<string>(words1).swap(words1);

    words1.shrink_to_fit(); // c++11

    print_size_and_capacity(words1, "words1 after reserve");

    vector<string> words2 = move(words1); // c++11

    print_size_and_capacity(words1, "words1");

    print_size_and_capacity(words2, "words2");

    vector<Person> vec_people;

    vec_people.push_back(Person(1, "Kowalski"));
    vec_people.emplace_back(2, "Nowak");  // forwarding konstruktora Person(2, "Nowak")

    print(vec_people, "vec_people");

    // specjalizacja dla wartości typu bool
    vector<bool> vec_bool(8);
    vec_bool[0] = 1;
    vec_bool[2] = true;

    print(vec_bool, "vec_bool");

    vec_bool.flip();

    print(vec_bool, "vec_bool");
}

