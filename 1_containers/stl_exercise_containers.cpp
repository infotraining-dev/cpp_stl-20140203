#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <cassert>
#include <list>

using namespace std;

void avg(int array[], size_t size)
{
    double sum = 0.0;
    for(size_t i = 0; i < size; ++i)
        sum += array[i];

    cout << "AVG: " << sum / size << endl;
}

bool is_even(int n)
{
    return n % 2 == 0;
}

int main()
{
    int tab[100];

    for(int i = 0; i < 100; ++i)
        tab[i] = rand() % 100;

    cout << "tab: ";
    for(int i = 0; i < 100; ++i)
        cout << tab[i] << " ";
    cout << "\n\n";

    // 1 - utwórz wektor vec_int zawierający kopie wszystkich elementów z  tablicy tab
    vector<int> vec_int(tab, tab+100);

    // 2 - wypisz wartość średnią przechowywanych liczb w vec_int
    avg(&vec_int[0], vec_int.size());

    // 3a - utwórz kopię wektora vec_int o nazwie vec_cloned
    vector<int> vec_cloned(vec_int);  // copy ctor

    // 3b - wyczysć kontener vec_int i zwolnij pamięć po buforze wektora
    vec_int.clear();
    vector<int>(vec_int).swap(vec_int); // lub vec_int.shrink_to_fit()

    int rest[] = { 1, 2, 3, 4 };
    // 4 - dodaj na koniec wektora vec_cloned zawartość tablicy rest
    vec_cloned.reserve(vec_cloned.size() + 4);
    vec_cloned.insert(vec_cloned.end(), rest, rest + 4);

    // 5 - posortuj zawartość wektora vec_cloned
    sort(vec_cloned.begin(), vec_cloned.end());

    // 6 - wyświetl na ekranie zawartość vec_cloned za pomocą iteratorów
    cout << "\nvec_cloned: ";

    for(vector<int>::const_iterator it = vec_cloned.begin(); it != vec_cloned.end(); ++it)
        cout << *it << " ";

    // c++11
    //for(auto& item : vec_cloned)
    //    cout << item << " ";

    cout << "\n\n";

    // 7 - utwórz kontener numbers (wybierz odpowiedni typ biorąc pod uwagę następne polecenia)
    // zawierający kopie elementów vec_cloned
    // TODO
    list<int> numbers(vec_cloned.begin(), vec_cloned.end());

    // 8 - usuń duplikaty elementów przechowywanych w kontenerze
    numbers.sort();
    numbers.unique();

    // 9 - usuń liczby parzyste z kontenera
    numbers.remove_if(&is_even);

    // 10 - wyświetl elementy kontenera w odwrotnej kolejności
    cout << "numbers: ";
    for(list<int>::const_reverse_iterator rit = numbers.rbegin(); rit != numbers.rend(); ++rit)
        cout << *rit << " ";
    cout << "\n\n";

    // 11 - skopiuj dane z numbers do listy lst_numbers jednocześnie i wyczyść kontener numbers
    list<int> lst_numbers;
    lst_numbers.splice(lst_numbers.begin(), numbers);

    // 12 - wyświetl elementy kontenera lst_numbers w odwrotnej kolejności
    cout << "lst_numbers: ";
    for(list<int>::const_reverse_iterator rit = lst_numbers.rbegin();
        rit != lst_numbers.rend(); ++rit)
        cout << *rit << " ";
    cout << "\n\n";
}

