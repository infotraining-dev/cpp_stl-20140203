#include <iostream>
#include <list>
#include <algorithm>
#include <boost/assign.hpp>

using namespace std;

template <typename Container>
void print(const Container& cont, const string& prefix)
{
    cout << prefix << ": [ ";
    for(typename Container::const_iterator it = cont.begin(); it != cont.end(); it++)
        cout << *it << " ";
    cout << "]" << endl;
}

int main()
{
    using namespace boost::assign;

    list<int> lst_int1;
    lst_int1 += 1, 5, 3, 2, 9, 6, 7, 8, 7, 7;

    list<int>::iterator it1 = find(lst_int1.begin(), lst_int1.end(), 6);

    if (it1 != lst_int1.end())
    {
        cout << "Znalazlem " << *it1 << endl;

        int* ptr = &(*it1);
        int& ref = *it1;

        print(lst_int1, "lst_int1 before");

        cout << "*it1 = " << *it1 << "; *ptr = " << *ptr << "; ref = " << ref << endl;

        lst_int1.insert(it1, 13);

        print(lst_int1, "lst_int1 after");

        cout << "*it1 = " << *it1 << "; *ptr = " << *ptr << "; ref = " << ref << endl;
    }
    else
    {
        cout << "Brak szukanego elementu";
    }

    lst_int1.sort();

    print(lst_int1, "lst_int1 sorted");

    list<int> lst_int2;
    lst_int2 += 5, 6, 7, 8, 9, 10;

    lst_int1.merge(lst_int2);

    print(lst_int1, "lst_int1 po merge");
    print(lst_int2, "lst_int2 po merge");

    lst_int1.unique();

    print(lst_int1, "po unique");

    lst_int2.splice(lst_int2.end(), lst_int1, ++lst_int1.begin(), --lst_int1.end());

    print(lst_int1, "po splice");
    print(lst_int2, "po splice");
}

