#include <iostream>
#include <memory>
#include <cassert>

using namespace std;

int main()
{
    // obiekt bez semantyki wartosci
    auto_ptr<int> ptr1(new int(10));

    cout << "value = " << *ptr1 << endl;

    auto_ptr<int> copy_ptr1 = ptr1;  // copy ctor

    assert(ptr1.get() == NULL);

    // C++11 - semantyka przenoszenia
    unique_ptr<int> uptr1(new int(10));

    unique_ptr<int> copy_uptr1 = move(uptr1);

    assert(uptr1.get() == NULL);

} // delete dla obiektu na stercie

