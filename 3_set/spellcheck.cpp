#include <iostream>
#include <fstream>
#include <iterator>
#include <set>
#include <string>
#include <chrono>
#include <boost/tokenizer.hpp>
#include <boost/unordered_set.hpp>

using namespace std;

int main()
{
    // wszytaj zawartość pliku en.dict ("słownik języka angielskieog")
    // sprawdź poprawość pisowni następującego zdania:
    string input_text = "this is an exmple of snetence";

    boost::tokenizer<> tok(input_text);

    ifstream file_dict("en.dict");

    if (!file_dict)
    {
        cout << "Błąd otwarcia pliku..." << endl;
        exit(1);
    }

    istream_iterator<string> start(file_dict);
    istream_iterator<string> end;

    //set<string> dict;
    boost::unordered_set<string> dict(start, end, 128000);

    //dict.rehash(128000);
    //dict.max_load_factor(4.0);

    cout << "bucket_count: " << dict.bucket_count() << endl;
    cout << "load_factor: " << dict.load_factor() << endl;
    cout << "max_load_factor: " << dict.max_load_factor() << endl;

    cout << "\n";

    // sprawdzenie pisowni
    for(boost::tokenizer<>::iterator word_it = tok.begin();
        word_it != tok.end(); ++word_it)
    {
        if (!dict.count(*word_it))
        {
            cout << "Błąd: " << *word_it << endl;
        }
    }

    cout << "\n";

    cout << "size: " << dict.size() << endl;
    cout << "bucket_count: " << dict.bucket_count() << endl;
    cout << "load_factor: " << dict.load_factor() << endl;
    cout << "max_load_factor: " << dict.max_load_factor() << endl;
}


