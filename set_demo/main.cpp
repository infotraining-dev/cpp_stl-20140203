#include <iostream>
#include <set>

using namespace std;

template <typename Container>
void print(const Container& cont, const string& prefix)
{
    cout << prefix << ": [ ";
    for(typename Container::const_iterator it = cont.begin(); it != cont.end(); it++)
        cout << *it << " ";
    cout << "]" << endl;
}

struct Person
{
    int id;
    string name;

    Person(int id, const string& name) : id(id), name(name)
    {}

    bool operator<(const Person& p) const
    {
        return id < p.id;
    }
};

struct PersonByNameComparer
{
    bool operator()(const Person& p1, const Person& p2) const
    {
        return p1.name < p2.name;
    }
};

ostream& operator<<(ostream& out, const Person& p)
{
    out << "Id: " << p.id << " Name: " << p.name;
    return out;
}

int main()
{
    set<int> set_int1;

    set_int1.insert(6);
    set_int1.insert(3);
    set_int1.insert(8);
    set_int1.insert(9);

    print(set_int1, "set_int1");

    pair<set<int>::iterator, bool> result_of_insert = set_int1.insert(5);

    if (result_of_insert.second)
        cout << *(result_of_insert.first) << " zostało wstawione\n";
    else
        cout << *(result_of_insert.first) << " jest już w zbiorze\n";

    set<int, greater<int> > set_int_desc(set_int1.begin(), set_int1.end());

    print(set_int_desc, "set_int_desc");

    // operacje kontenerów asocjacyjnych

    set<int>::iterator pos = set_int1.find(7);

    if (pos == set_int1.end())
        cout << "Brak wartości 7 w zbiorze\n";
    else
        cout << *pos << " jest w zbiorze\n";

    if (!set_int1.count(7))
        cout << "Brak 7 w zbiorze" << endl;

    set_int1.erase(9); // usiniecie klucza rownowaznego 9

    multiset<int> mset_int(set_int1.begin(), set_int1.end());

    mset_int.insert(7);
    mset_int.insert(7);
    mset_int.insert(7);
    mset_int.insert(10);

    print(mset_int, "mset_int");

    pair<set<int>::iterator, set<int>::iterator> range = mset_int.equal_range(7);

    cout << "ilosc 7: " << mset_int.count(7) << endl;

    cout << "wszystkie 7: ";
    for(set<int>::iterator it = range.first; it != range.second; ++it)
    {
        cout << *it << " ";
    }
    cout << endl;

    set<Person, PersonByNameComparer> set_people;

    set_people.insert(Person(1, "Kowalski"));
    set_people.insert(Person(3, "Kowal"));
    set_people.insert(Person(2, "Nowak"));

    print(set_people, "set_people");
}

