#include <iostream>
#include <algorithm>
#include <vector>
#include <list>
#include <iterator>

using namespace std;

template <typename InIt, typename OutIt>
OutIt mcopy(InIt start, InIt end, OutIt out)
{
    for(; start != end; ++start)
    {
        *out = *start;  // push_back
        ++out;
    }

    return out;
}

template <typename Container>
void print(const Container& cont, const string& prefix)
{
    cout << prefix << ": [ ";
    for(typename Container::const_iterator it = cont.begin(); it != cont.end(); it++)
        cout << *it << " ";
    cout << "]" << endl;
}

int main()
{
    int tab[] = { 1, 2, 3, 4, 5, 6, 7 };
    vector<int> vec_int(7);

    mcopy(tab, tab + 7, vec_int.begin());

    back_insert_iterator<vector<int> > bit(vec_int);

    *bit = 10; // vec_int.push_back(10);
    *bit = 20;
    *bit = 30;

    print(vec_int, "vec_int");

    list<int> lst_int;

    //front_insert_iterator<list<int> > fit(lst_int);
    //copy(vec_int.begin(), vec_int.end(), fit);

    copy(vec_int.begin(), vec_int.end(), front_inserter(lst_int));

    print(lst_int, "lst_int");

    //insert_iterator<vector<int> > insit(vec_int, vec_int.begin() + 3);
    //copy(lst_int.begin(), lst_int.end(), insit);

    copy(lst_int.begin(), lst_int.end(), inserter(vec_int, vec_int.begin() + 3));

    print(vec_int, "vec_int");


    // iteratory wejścia-wyjścia

    istream_iterator<int> start(cin);
    istream_iterator<int> end;

    vector<int> vec_int2(start, end);

    cout << "Wprowadzone elementy:" << endl;
    copy(vec_int2.begin(), vec_int2.end(), ostream_iterator<int>(cout, " "));
    cout << "\n";
}

