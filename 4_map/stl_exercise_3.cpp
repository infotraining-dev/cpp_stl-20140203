#include <map>
#include <iostream>
#include <fstream>
#include <iterator>
#include <set>
#include <string>
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;

/*
    Napisz program zliczający ilosc wystapien danego slowa w pliku tekstowym.
    Wyswietl wyniki zaczynajac od najczesciej wystepujacych slow.
*/

typedef vector<string> WordContainerType;

WordContainerType load_words_from_book(const string& file_name)
{
    ifstream file_book(file_name);

    if (!file_book)
    {
        cout << "Błąd otwarcia pliku " << file_name << endl;
        exit(1);
    }

    string line;
    WordContainerType words;

    while (getline(file_book, line))
    {
        boost::tokenizer<> tok(line);

        words.insert(words.end(), tok.begin(), tok.end());
    }

    return words;
}

int main()
{   
    typedef map<string, int> ConcordanceType;

    WordContainerType words = load_words_from_book("holmes.txt");

    // zliczenie ilości słów
    ConcordanceType concordance;

    for(WordContainerType::const_iterator it = words.begin();
        it != words.end(); ++it)
    {
        ++concordance[boost::to_lower_copy(*it)];
    }

    // wyświetlenie 10 najczęściej występujących
    typedef multimap<int, string, greater<int> > ResultType;
    ResultType result;

    for(ConcordanceType::iterator it = concordance.begin();
        it != concordance.end(); ++it)
    {
        result.insert(make_pair(it->second, it->first));
    }

    int count = 0;
    for(ResultType::iterator it = result.begin(); it != result.end(); ++it, ++count)
    {
        if (count == 10)
            break;

        cout << it->second << " - " << it->first << "\n";
    }
}
