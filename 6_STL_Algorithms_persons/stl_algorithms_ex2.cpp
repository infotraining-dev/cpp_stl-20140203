#include "person.hpp"
#include "utils.hpp"

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>
#include <fstream>
#include <boost/bind.hpp>

using namespace std;


template <typename T>
class Accumulator : public unary_function<T, void>
{
    T sum_;
public:
    Accumulator(const T& init = T()) : sum_(init) {}

    void operator()(const T& x)
    {
        sum_ += x;
    }

    T value() const
    {
        return sum_;
    }
};


int main()
{
    vector<Person> employees;
    fill_person_container(employees);

    cout << "Wszyscy pracownicy:\n";
    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));
    cout << endl;

    // wyświetl pracownikow z pensją powyżej 3000
    cout << "\nPracownicy z pensja powyżej 3000:\n";
//    copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
//            boost::bind(&Person::salary, _1) > 3000.0);

    double threshold = 3000.0;
    copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
            [=](const Person& p) { return p.salary() > threshold; });

    // wyświetl pracowników o wieku poniżej 30 lat
    cout << "\nPracownicy o wieku poniżej 30 lat:\n";
    copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
            boost::bind(&Person::age, _1) < 30);

    // posortuj malejąco pracownikow wg nazwiska
    cout << "\nLista pracowników wg nazwiska (malejaco):\n";
//    sort(employees.begin(), employees.end(),
//         boost::bind(&Person::name, _1) > boost::bind(&Person::name, _2));

    auto comp_by_name =
            [](const Person& p1, const Person& p2) { return p1.name() > p2.name(); };

    sort(employees.begin(), employees.end(),
         comp_by_name);
    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));

    // wyświetl kobiety
    cout << "\nKobiety:\n";
    copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
            boost::bind(&Person::gender, _1) == Female);


    // ilość osob zarabiajacych powyżej średniej
    cout << "\nIlosc osob zarabiajacych powyzej sredniej:\n";

    double avg = accumulate(employees.begin(), employees.end(), 0.0,
                            boost::bind(
                                plus<double>(),
                                    _1,
                                    boost::bind(&Person::salary, _2))) / employees.size();

    Accumulator<double> acc;

    for_each(employees.begin(), employees.end(),
             boost::bind(boost::ref(acc), boost::bind(&Person::salary, _1)));

    cout << "Avg: " << acc.value() / employees.size() << endl;

    cout << "Avg: " << avg << endl;

    cout << "Ilosc > Avg: " << count_if(employees.begin(), employees.end(),
                                        boost::bind(&Person::salary, _1) > avg) << endl;

    ofstream fout("employees.txt");

    for_each(employees.begin(), employees.end(),
             boost::bind(&Person::write, _1, boost::ref(fout)));
}
