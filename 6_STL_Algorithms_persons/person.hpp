#ifndef PERSON_HPP
#define PERSON_HPP

#include <string>
#include <iostream>
#include <vector>

enum Gender { Male, Female };

class Person
{
	std::string name_;
	unsigned int age_;
	double salary_;
	Gender gender_;
public:
	Person(const std::string& name, unsigned int age, double salary, Gender gender);

	std::string name() const;
	void set_name(const std::string& name);

	unsigned int age() const;
	void set_age(unsigned int age);

	double salary() const;
	void set_salary(double salary);

	Gender gender() const;
	void set_gender(Gender gender);

	void print() const;

    void write(std::ostream& out)
    {
        out << "Person: " << name_ << std::endl;
    }
};

std::ostream& operator <<(std::ostream& out, const Person& p);

template <typename Container>
void fill_person_container(Container& container)
{
	container.push_back(Person("Gruszka", 45, 3000.0, Male));
	container.push_back(Person("Malinowska", 33, 5500.0, Female));
	container.push_back(Person("Brzozowski", 28, 2000.0, Male));
	container.push_back(Person("Wierzbowski", 54, 7900.0, Male));
	container.push_back(Person("Kowalski", 44, 4300.0, Male));
	container.push_back(Person("Kowalewska", 23, 2450.0, Female));
	container.push_back(Person("Nowak", 58, 4900.0, Male));
	container.push_back(Person("Kowal", 29, 3700.0, Male));
	container.push_back(Person("Zawadzka", 64, 5300.0, Female));
	container.push_back(Person("Hoffander", 53, 7600.0, Male));
	container.push_back(Person("Paluch", 53, 2000.0, Male));
	container.push_back(Person("Pruski", 41, 9000.0, Male));
};

#endif
