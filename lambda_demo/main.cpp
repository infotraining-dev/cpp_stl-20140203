#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

int main()
{
    vector<int> vec1 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    transform(vec1.begin(), vec1.end(), ostream_iterator<int>(cout, " "),
              [](int x) { return x * x; });

    cout << "\n";

    int external = 5;

    transform(vec1.begin(), vec1.end(), ostream_iterator<int>(cout, " "),
              [=](int x) { return x * external; });

    cout << "\n";

    int sum = 0;

    for_each(vec1.begin(), vec1.end(), [&sum, external](int x) { sum += external * x;});

    cout << "sum: " << sum << endl;
}

