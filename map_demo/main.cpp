#include <iostream>
#include <map>

using namespace std;

template <typename Container>
void print(const Container& cont, const string& prefix)
{
    cout << prefix << ": [ ";
    for(typename Container::const_iterator it = cont.begin(); it != cont.end(); it++)
        cout << *it << " ";
    cout << "]" << endl;
}

template <typename T1, typename T2>
ostream& operator<<(ostream& out, const pair<T1, T2>& p)
{
    out << "(" << p.first << ", " << p.second << ")";
    return out;
}

int main()
{
    map<int, string> days_of_week;
    //map<int, string> days_of_week = { { 2, "Wt" }, { 7, "Nd" } }; // C++11

    days_of_week.insert(map<int, string>::value_type(1, "Pon"));
    days_of_week.insert(pair<int, string>(5, "Pt"));
    days_of_week.insert(make_pair(4, "Czw"));

    cout << days_of_week[4] << endl;

    map<int, string>::iterator pos = days_of_week.find(3);
    if (pos != days_of_week.end())
        cout << *pos << endl;
    else
        cout << "Brak elementu o kluczu 3" << endl;

    for(map<int, string>::const_iterator it = days_of_week.begin();
        it != days_of_week.end(); ++it)
    {
        cout << it->first << " - " << it->second << endl;
    }

    print(days_of_week, "days_of_week");
}

