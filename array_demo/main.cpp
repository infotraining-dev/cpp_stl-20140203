#include <iostream>
#include <boost/array.hpp>

using namespace std;

template <typename Container>
void print(const Container& cont, const string& prefix)
{
    cout << prefix << ": [ ";
    for(typename Container::const_iterator it = cont.begin(); it != cont.end(); it++)
        cout << *it << " ";
    cout << "]" << endl;
}

void legacy(int* tab, unsigned int size)
{
    for(int* it = tab; it != tab + size; ++it)
        cout << *it << " ";
    cout << endl;
}

int main()
{
    int tab1[10] = {1, 3, 4, 5};

    boost::array<int, 10> arr1 = {};

    cout << "arr1.size() = " << arr1.size() << endl;

    arr1[0] = 10;
    arr1[3] = 5;

    print(arr1, "arr1");

    try
    {
        arr1.at(100) = 10;
    }
    catch(const out_of_range& e)
    {
        cout << e.what() << endl;
    }

    boost::array<int, 10> arr2 = arr1;

    arr1[6] = -1;

    print(arr2, "arr2");

    print(arr1, "arr1");
    print(arr2, "arr2");

    cout << "przed swap:\n";

    arr1.swap(arr2);

    cout << "po swap:\n";

    print(arr1, "arr1");
    print(arr2, "arr2");

    legacy(arr1.data(), arr1.size());
}

