#include <iostream>
#include <boost/assign.hpp>
#include <deque>

using namespace std;

template <typename Container>
void print(const Container& cont, const string& prefix)
{
    cout << prefix << ": [ ";
    for(typename Container::const_iterator it = cont.begin(); it != cont.end(); it++)
        cout << *it << " ";
    cout << "]" << endl;
}

int main()
{
    using namespace boost::assign;

    vector<int> vec_int;
    vec_int += 1, 2, 3, 4, 5, 6, 7, 8, 9, 10;

    deque<int> dq_int(vec_int.begin(), vec_int.end());

    int& ref = dq_int[0];

    int* ptr = &dq_int[5];

    cout << "ref = " << ref << " *ptr = " << *ptr << endl;

    dq_int.push_front(-1);

    //dq_int.insert(dq_int.begin() + 3, 0);  // inwalidacja it, &, *

    dq_int.push_back(11);

    print(dq_int, "dq_int");

    cout << "ref = " << ref << " *ptr = " << *ptr << endl;
}

