#include <iostream>
#include <boost/bind.hpp>

using namespace std;

void foo(int x, const string& txt, double dx)
{
    cout << "foo(" << x << ", " << txt << ", " << dx << ")\n";
}

class Add //: public binary_function<int, int, int>
{
public:
    int operator()(int x, int y) const
    {
        return x + y;
    }
};

class Person
{
    int id_;
    string name_;
public:


    Person(int id, const string& name) : id_(id), name_(name)
    {}

    void print(const string& prefix) const
    {
        cout << prefix << id_ << " - " << name_ << "\n";
    }
};

int main()
{
    auto bound_foo = boost::bind(&foo, _1, "text", _2);

    bound_foo(3.14, 1); // foo(13, "text", 3.14)
    bound_foo(6.28, 2); // foo(13, "another text", 3.14)

    Add add;

    auto bound_add = boost::bind<int>(add, _1, 2);

    cout << bound_add(10) << endl;

    Person p(1, "Kowalski");

    auto bound_method = boost::bind(&Person::print, &p, "Person: ");

    bound_method();

    auto lazy_bound = std::bind(&Person::print, placeholders::_1, "Person: ");

    lazy_bound(p);
}
