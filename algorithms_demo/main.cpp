#include <iostream>
#include <string>
#include <algorithm>
#include <functional>
#include <iterator>
#include <boost/assign.hpp>
#include <vector>

using namespace std;

template <typename Container>
void print(const Container& cont, const string& prefix)
{
    cout << prefix << ": [ ";
    for(typename Container::const_iterator it = cont.begin(); it != cont.end(); it++)
        cout << *it << " ";
    cout << "]" << endl;
}

template <typename T>
class Accumulator : public unary_function<T, void>
{
    T sum_;
public:
    Accumulator(const T& init = T()) : sum_(init) {}

    void operator()(const T& x)
    {
        sum_ += x;
    }

    T value() const
    {
        return sum_;
    }
};

int main()
{
    using namespace boost::assign;

    vector<int> vec1;
    vec1 += 1, 7, 0, 2, 10, 40, 22, 22, 244, -4, 3, -8, 7, 10;

    print(vec1, "vec1");

    Accumulator<int> acc = for_each(vec1.begin(), vec1.end(), Accumulator<int>());

    cout << "sum of elements: " << acc.value() << endl;

    vector<int>::iterator where = max_element(vec1.begin(), vec1.end());

    cout << "max = " << *where << endl;

    where = search_n(vec1.begin(), vec1.end(), 2, 22);

    if (where != vec1.end())
        cout << "index: " << (where - vec1.begin()) << endl;

//    fill(vec1.begin(), vec1.end(), -1);

//    print(vec1, "vec1");

//    generate(vec1.begin(), vec1.end(), rand);

//    print(vec1, "vec1");

    //remove(vec1.begin(), vec1.end(), 10);
    vec1.erase(remove_if(vec1.begin(), vec1.end(), bind2nd(greater<int>(), 5)), vec1.end());

    print(vec1, "vec1 po remove > 5");

    vector<int> vec1_neg(vec1.size());

    transform(vec1.begin(), vec1.end(), vec1_neg.begin(), negate<int>());

    print(vec1_neg, "vec1_neg");

    transform(vec1.begin(), vec1.end(), vec1_neg.begin(),
              ostream_iterator<int>(cout, " "), plus<int>());
    cout << "\n";

    sort(vec1.begin(), vec1.end());
    sort(vec1_neg.begin(), vec1_neg.end());

    list<int> merged;
    merge(vec1.begin(), vec1.end(), vec1_neg.begin(), vec1_neg.end(), back_inserter(merged));

    print(merged, "merged");

    cout << "intersection: ";
    set_intersection(vec1.begin(), vec1.end(), vec1_neg.begin(), vec1_neg.end(),
                     ostream_iterator<int>(cout, " "));
    cout << "\n";

}

