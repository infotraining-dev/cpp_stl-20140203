#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <ctime>
#include <algorithm>
#include <cstring>

using namespace std;

/*
    Utwórz trzy sekwencje typu int: vector, deque i list. Wypelnij je wartosciami losowymi.
    Porównaj czasy tworzenia i wypelniania danymi sekwencji. Napisz szablon funkcji sortującej sekwencje
    vector i deque. Napisz specjalizowany szablon funkcji realizującej sortowanie dla kontenera list.
    Porównaj efektywność operacji sortowania.
*/

template <typename Container, typename Func>
void fill_container(Container& c, Func f, size_t n)
{
    for(size_t i = 0; i < n; ++i)
        c.push_back(f());
}

// szablony funkcji umożliwiające sortowanie kontenera
template <typename Container>
void sort(Container& cont)
{
    sort(cont.begin(), cont.end());
}

template <typename T>
void sort(list<T>& lst)
{
    lst.sort();
}

clock_t get_clock()
{
    clock_t ct = clock();

    if (ct == clock_t(-1))
    {
        cerr << "Clock error" << endl;
        exit(1);
    }

    return ct;
}

int main()
{
    const int n = 10000000;

    clock_t t1, t2;

    // utwórz kontener
    //typedef vector<int> ContType;
    //typedef deque<int> ContType;
    typedef list<int> ContType;
    ContType c;

    cout << "Preparing data... Filling container." << endl;

    t1 = get_clock();

    // wypełnij kontener danymi
    fill_container(c, rand, n);

    t2 = get_clock();

    cout << "Time: " << double(t2-t1)/CLOCKS_PER_SEC << "sec." << endl;

    cout << "\nStart sorting..." << endl;

    t1 = get_clock();

    // posortuj kontener
    sort(c);

    t2 = get_clock();

    cout << "Time: " << double(t2-t1)/CLOCKS_PER_SEC << "sec." << endl;
}
